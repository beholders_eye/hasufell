# Copyright 2018 Julian Ospald <hasufell@posteo.de>
# Distributed under the terms of the GNU General Public License v2

require github [ user=haiwen tag="v${PV}" ]
require cmake [ api=2 ]
require gtk-icon-cache

SUMMARY="Seafile desktop client"
DESCRIPTION="
Seafile is an enterprise file hosting platform with high reliability and
performance. Put files on your own server. Sync and share files across different
devices, or access all the files as a virtual disk.
"
HOMEPAGE="https://www.seafile.com ${HOMEPAGE}"

LICENCES="Apache-2.0"
SLOT="0"
PLATFORMS="~amd64 ~x86"
MYOPTIONS="
    ( providers: libressl openssl ) [[ number-selected = exactly-one ]]
"

DEPENDENCIES="
    build:
        virtual/pkg-config
    build+run:
        dev-db/sqlite:3
        dev-libs/glib:2
        dev-libs/jansson
        dev-libs/libevent:=
        net-libs/libsearpc
        net-misc/seafile[~${PV}]
        sys-libs/zlib
        x11-libs/qtbase:5
        providers:libressl? ( dev-libs/libressl:= )
        providers:openssl?  ( dev-libs/openssl )
"

BUGS_TO="hasufell@posteo.de"

DEFAULT_SRC_PREPARE_PATCHES=(
    "${FILES}"/select-qt5.patch
    "${FILES}"/0001-Fix-build-with-libressl-2.7.x.patch
)
