# Copyright 2019 Julian Ospald <hasufell@posteo.de>
# Distributed under the terms of the GNU General Public License v2

# Purpose: an exlib dealing with installation of GOG games
# Maintainer: Julian Ospald <hasufell@posteo.de>
# Exports: pkg_nofetch, pkg_setup, src_unpack
# Side-effects: - adds build dependencies
#               - sets $WORK, $DOWNLOADS, $LICENSES, $RESTRICT and $HOMEPAGE
#
# Usage
#   exparams: - installer: the shell script installer (required)
#             - unzip: whether to unzip and add unzip dependency (default: true)
#             - install_dir: installation dir (default: /opt/${PN})
#  variables: - GOG_SH: set to exparam installer
#             - GOG_INSTALL_DIR: set to exparam install_dir, use this in src_install


myexparam installer=
myexparam -b unzip=true
myexparam install_dir=/opt/${PN}

exparam -v GOG_SH installer
exparam -v GOG_INSTALL_DIR install_dir

if [[ -z $(exparam installer) ]]; then
    die "installer needs to be set!"
fi

if [[ "$GOG_INSTALL_DIR" != /* ]]; then
    die "${GOG_INSTALL_DIR} must be absolute!"
fi

export_exlib_phases pkg_nofetch pkg_setup src_unpack


HOMEPAGE="https://www.gog.com"
DOWNLOADS="manual: ${GOG_SH}"

LICENCES="all-rights-reserved GOG-EULA"
RESTRICT="fetch"

if exparam -b unzip ; then
    DEPENDENCIES+="
        build:
            virtual/unzip
    "
fi

WORK="${WORKBASE}/data/noarch"

# Exported 'pkg_nofetch'.
#
# Arguments: none
# Side-effects: prints einfo
gog_pkg_nofetch() {
    einfo
    einfo "Please buy & download \"${GOG_SH}\""
    einfo "from:"
    einfo "  ${HOMEPAGE}"
    einfo "and move/link it to \"${FETCHEDDIR}\""
    einfo
}

# Exported 'pkg_setup'.
#
# Arguments: none
# Side-effects: makes sure /opt is allowed to be installed into
gog_pkg_setup() {
    exdirectory --allow "$(dirname "${GOG_INSTALL_DIR}")"
}

# Exported 'src_unpack'.
#
# Arguments: none
# Side-effects: unpacks the data
gog_src_unpack() {
    if exparam -b unzip ; then
        unzip -qo "${FETCHEDDIR}/${GOG_SH}"
        [[ $? -le 1 ]] || die "unpacking ${GOG_SH} failed!"
    fi
}
